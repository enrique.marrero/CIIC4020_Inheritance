package orderedStructures;

public class Arithmetic extends Progression {
	private double commonDifference; 
	
	public Arithmetic(double firstValue, double commonDifference) { 
		super(firstValue); 
		this.commonDifference = commonDifference; 
	}
	
	@Override
	public double nextValue() {
		if(!flag) {
			throw new IllegalStateException("First value!");
		}
		current = current + commonDifference; 
		return current;
	}
	@Override
	public String toString() {
		return "Arith("+ (int)this.firstValue() +","+ (int)commonDifference+")";
	}
	
	public double getTerm(int n){
		double term=firstValue() + (this.commonDifference*(n-1));
		return term;
	}


}
